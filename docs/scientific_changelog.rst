Scientific Changelog
####################

This page distills the verbosity of the :doc:`full Changelog <changelog>` into only those changes that affect the
scientific quality of the L1 data.

.. changelog::
    :towncrier:
    :towncrier-skip-if-empty:
    :changelog_file: ../SCIENCE_CHANGELOG.rst
