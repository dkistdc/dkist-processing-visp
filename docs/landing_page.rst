Overview
========

The mission of the Visible Spectropolarimeter (ViSP) is to measure the full state of polarization
simultaneously at diverse wavelengths in the visible spectrum, resolving the profiles of spectral
lines from the solar atmosphere. The ViSP is a scanning slit spectrograph.

The `dkist-processing-visp` code repository contains the implementation of the ViSP calibration
pipelines, which convert Level 0 files from the telescope into Level 1 output products. Pipelines
are built on `dkist-processing-common <https://docs.dkist.nso.edu/projects/common/>`_ tasks
and use the `dkist-processing-core <https://docs.dkist.nso.edu/projects/core/>`_ framework.
Follow links on this page for more information about calibration pipelines.

Level 1 data products are available at the `DKIST Data Portal <https://dkist.data.nso.edu/>`_ and
can be analyzed with `DKIST python user tools <https://docs.dkist.nso.edu/projects/python-tools/>`_.
For help, please contact the `DKIST Help Desk <https://nso.atlassian.net/servicedesk/customer/portals/>`_.
