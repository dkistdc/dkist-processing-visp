"""Controlled list of visp-specific task tag names."""
from enum import Enum


class VispTaskName(str, Enum):
    """Controlled list of task tag names."""

    background = "BACKGROUND"
