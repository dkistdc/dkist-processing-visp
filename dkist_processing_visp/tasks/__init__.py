"""init."""
from dkist_processing_visp.tasks.assemble_movie import *
from dkist_processing_visp.tasks.background_light import *
from dkist_processing_visp.tasks.dark import *
from dkist_processing_visp.tasks.geometric import *
from dkist_processing_visp.tasks.instrument_polarization import *
from dkist_processing_visp.tasks.l1_output_data import *
from dkist_processing_visp.tasks.lamp import *
from dkist_processing_visp.tasks.make_movie_frames import *
from dkist_processing_visp.tasks.parse import *
from dkist_processing_visp.tasks.quality_metrics import *
from dkist_processing_visp.tasks.science import *
from dkist_processing_visp.tasks.solar import *
from dkist_processing_visp.tasks.write_l1 import *
